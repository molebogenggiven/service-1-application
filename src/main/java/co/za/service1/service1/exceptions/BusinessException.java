package co.za.service1.service1.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private  final String message;
    private final HttpStatus httpStatus;
    private final LocalDateTime localDateTime;
    public BusinessException(String message, HttpStatus httpStatus, LocalDateTime localDateTime){
        this.message = message;
        this.httpStatus = httpStatus;
        this.localDateTime = localDateTime;
    }
}
