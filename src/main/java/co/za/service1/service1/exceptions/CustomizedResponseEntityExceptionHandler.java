package co.za.service1.service1.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestController
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<Object> handleAllException(BusinessException exception, WebRequest webRequest){

        ExceptionResponse exceptionResponse = new ExceptionResponse();
        exceptionResponse.setDetails(webRequest.getDescription(false));
        exceptionResponse.setTimeStamp(exception.getLocalDateTime());
        exceptionResponse.setMessage(exception.getMessage());

        return new ResponseEntity(exceptionResponse, exception.getHttpStatus());
    }
}
