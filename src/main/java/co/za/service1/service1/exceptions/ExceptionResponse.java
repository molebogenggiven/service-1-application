package co.za.service1.service1.exceptions;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class ExceptionResponse {

    private LocalDateTime timeStamp;
    private String message;
    private String details;
}
