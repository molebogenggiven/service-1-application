package co.za.service1.service1.service;

import co.za.service1.service1.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitMQSender {


    @Autowired
    private AmqpTemplate rabbitTemplate;
    @Autowired
    private Queue queue;

    public void send(String userDto) {
        rabbitTemplate.convertAndSend(queue.getName(), userDto);

        log.info("Sending Message to the Queue  {}", userDto);
    }
}
