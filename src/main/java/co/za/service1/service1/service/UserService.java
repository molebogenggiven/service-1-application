package co.za.service1.service1.service;

import co.za.service1.service1.dto.UserDto;
import org.springframework.stereotype.Service;


public interface UserService {

    void createUser(UserDto userDto);
    void deleteUser(String userId);
    void updateUser(UserDto userDto);
}
