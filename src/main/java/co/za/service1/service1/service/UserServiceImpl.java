package co.za.service1.service1.service;

import co.za.service1.service1.dto.UserDto;
import co.za.service1.service1.exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserServiceImpl implements UserService{


    @Autowired
    private RabbitMQSender rabbitMQSender;

    @Override
    public void createUser(UserDto userDto) {

        if (userDto.getName().isBlank())
            throw new BusinessException("Name cannot be null", HttpStatus.NOT_ACCEPTABLE,
                    LocalDateTime.now());

        rabbitMQSender.send(userDto.toString());

    }

    @Override
    public void deleteUser(String userId) {
        if (userId.isBlank())
            throw new BusinessException("UserId cannot be null", HttpStatus.NOT_ACCEPTABLE,
                    LocalDateTime.now());

        rabbitMQSender.send(userId);

    }

    @Override
    public void updateUser(UserDto userDto) {

        if (userDto.getName().isBlank())
            throw new BusinessException("Name cannot be null", HttpStatus.NOT_ACCEPTABLE,
                    LocalDateTime.now());

        rabbitMQSender.send(userDto.toString());
    }
}
