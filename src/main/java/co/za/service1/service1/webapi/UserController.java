package co.za.service1.service1.webapi;


import co.za.service1.service1.dto.UserDto;
import co.za.service1.service1.exceptions.BusinessException;
import co.za.service1.service1.service.RabbitMQSender;
import co.za.service1.service1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("api/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create_user")
    public ResponseEntity<String> createUser(@RequestBody UserDto userDto){

        try{
            userService.createUser(userDto);
            return new ResponseEntity<>("Sent create user message", HttpStatus.OK);
        }catch (Exception e){
            throw new BusinessException("Failed to send create user message",
                    HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now());
        }


    }


    @PutMapping("/create_user")
    public ResponseEntity<String> updateUser(@RequestBody UserDto userDto){

        try{
            userService.updateUser(userDto);
            return new ResponseEntity<>("Sent updated user message", HttpStatus.OK);
        }catch (Exception e){

            throw new BusinessException("Failed to send update user message",
                    HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now());
        }

    }

    @DeleteMapping("/delete_user/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable String userId){

        try{
            userService.deleteUser(userId);
            return new ResponseEntity<>("Sent delete user message", HttpStatus.OK);
        }catch (Exception e){

            throw new BusinessException("Failed to send delete user message",
                    HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now());

        }



    }
}
